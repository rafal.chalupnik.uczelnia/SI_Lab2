﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Lab2
{
    public class GeneticAlgorithm
    {
        private readonly Evaluator evaluator_;
        private ILogger logger_;
        private List<int[]> population_;
        private readonly List<List<int[]>> populationHistory_;
        private readonly Random random_;

        public Statistics CalculateStatistics()
        {
            var bestScore = int.MaxValue;
            var worseScore = int.MinValue;
            var scoreSum = 0;
            int[] bestSolution = null;

            foreach (var solution in population_)
            {
                var score = evaluator_.Evaluate(solution);
                if (score < bestScore)
                {
                    bestScore = score;
                    bestSolution = solution;
                }
                if (score > worseScore)
                    worseScore = score;
                scoreSum += score;
            }

            return new Statistics(bestSolution, bestScore, scoreSum / population_.Count, worseScore);
        }
        public Tuple<int[], int[]> Cross(int[] _parent1, int[] _parent2, int _crossPoint)
        {
            if (_parent1 == null || _parent2 == null)
                throw new ArgumentException("Parents cannot be null!");
            if (_parent1.Length != _parent2.Length)
                throw new ArgumentException("Parents must have the same length!");
            if (_crossPoint >= _parent1.Length)
                throw new ArgumentException("Invalid cross point!");

            var size = _parent1.Length;
            var child1 = new int[size];
            var child2 = new int[size];

            for (var i = 0; i < size; i++)
            {
                child1[i] = i < _crossPoint ? _parent1[i] : _parent2[i];
                child2[i] = i < _crossPoint ? _parent2[i] : _parent1[i];
            }

            if (!ValidateSolution(child1))
                child1 = RepairSolution(child1);
            if (!ValidateSolution(child2))
                child2 = RepairSolution(child2);

            return Tuple.Create(child1, child2);
        }
        private int[] GenerateRandomSolution(int _length)
        {
            var available = Enumerable.Range(0, _length).ToList();

            var solution = new int[_length];
            for (var i = 0; i < _length; i++)
            {
                solution[i] = available[random_.Next(available.Count)];
                available.Remove(solution[i]);
            }

            return solution;
        }

        private Tuple<int[], int[]> GetParentsByRoulette(Statistics _statistics, Random _random)
        {
            var roulette = new List<int>();
            var worse = _statistics.WorseScore;

            for (var i = 0; i < population_.Count; i++)
            {
                var fitness = worse - evaluator_.Evaluate(population_[i]);
                for (var j = 0; j < fitness; j++)
                    roulette.Add(i);
            }

            var firstParent = population_[roulette[_random.Next(roulette.Count)]];
            var secondParent = population_[roulette[random_.Next(roulette.Count)]];

            return Tuple.Create(firstParent, secondParent);
        }
        private int[] GetParentByTournament()
        {
            var candidate1 = population_[random_.Next(population_.Count)];
            var candidate2 = population_[random_.Next(population_.Count)];
            return evaluator_.Evaluate(candidate1) < evaluator_.Evaluate(candidate2) ? candidate1 : candidate2;
        }
        private void Initialize(int _initialPopulation)
        {
            for (var i = 0; i < _initialPopulation; i++)
                population_.Add(GenerateRandomSolution(evaluator_.Size));
        }
        private int[] Mutate(int[] _solution, double _chance)
        {
            if (random_.NextDouble() > _chance)
            {
                var indexes = Enumerable.Range(0, _solution.Length).ToList();
                var index1 = indexes[random_.Next(indexes.Count)];
                indexes.Remove(index1);
                var index2 = indexes[random_.Next(indexes.Count)];

                _solution = Utils.Swap(_solution, index1, index2);
            }
            return _solution;
        }
        private int[] RepairSolution(int[] _solution)
        {
            var notOccured = Enumerable.Range(0, evaluator_.Size).ToList();
            var alreadyOccured = new List<int>();

            for (var i = 0; i < evaluator_.Size; i++)
            {
                if (alreadyOccured.Contains(_solution[i]))
                    _solution[i] = notOccured[random_.Next(0, notOccured.Count)];

                alreadyOccured.Add(_solution[i]);
                notOccured.Remove(_solution[i]);
            }

            return _solution;
        }

        private bool ValidateSolution(int[] _solution)
        {
            var listSolution = _solution.ToList();
            return listSolution.Count == listSolution.Distinct().Count();
        }

        public GeneticAlgorithm(Evaluator _evaluator, ILogger _logger, int? _seed = null)
        {
            evaluator_ = _evaluator ?? throw new ArgumentNullException(nameof(_evaluator), "Evaluator cannot be null!");
            logger_ = _logger;
            random_ = _seed == null ? new Random() : new Random(_seed.Value);
            population_ = new List<int[]>();
            populationHistory_ = new List<List<int[]>>();
        }

        public Result Start(int _seconds, int _initialPopulation, double _mutationChance, bool _useRoulette, bool _enhancedRoulette = false)
        {
            if (_seconds <= 0)
                throw new ArgumentException("At least one iteration must be performed!", nameof(_seconds));
            if (_initialPopulation < 2)
                throw new ArgumentException("At least two solutions must exist in initial population!", nameof(_initialPopulation));
            if (_mutationChance < 0.0 || _mutationChance > 1.0)
                throw new ArgumentException("MutationChance must have a value between 0.0 and 1.0!", nameof(_mutationChance));

            Initialize(_initialPopulation);
            var statistics = CalculateStatistics();
            logger_.LogIteration(0, statistics);
            var i = 0;
            var watch = Stopwatch.StartNew();
            var milliseconds = _seconds * 1000;
            while (watch.ElapsedMilliseconds < milliseconds)
            {
                i++;
                var newPopulation = new List<int[]>();

                var roulette = new List<int>();

                if (_useRoulette)
                {
                    for (var j = 0; j < population_.Count; j++)
                    {
                        var fitness = _enhancedRoulette
                            ? statistics.WorseScore - evaluator_.Evaluate(population_[i])
                            : (1.0 / evaluator_.Evaluate(population_[j])) * 10000;
                        for (var k = 0; k < fitness; k++)
                            roulette.Add(j);
                    }
                }

                for (var j = 0; j < _initialPopulation / 2; j++)
                {
                    var parent1 = _useRoulette
                        ? population_[roulette[random_.Next(roulette.Count)]]
                        : GetParentByTournament();

                    var parent2 = _useRoulette
                        ? population_[roulette[random_.Next(roulette.Count)]]
                        : GetParentByTournament();

                    if (Utils.AreIntArraysEqual(parent1, parent2))
                        j--;
                    else
                    {
                        // Reprodukcja
                        var children = Cross(parent1, parent2, random_.Next(evaluator_.Size));
                        newPopulation.Add(children.Item1);
                        newPopulation.Add(children.Item2);
                    }
                }

                // Mutacja
                for (var j = 0; j < newPopulation.Count; j++)
                {
                    var mutated = Mutate(newPopulation[j], _mutationChance);
                    if (Utils.AreIntArraysEqual(newPopulation[j], mutated))
                        newPopulation[j] = mutated;
                }

                statistics = CalculateStatistics();
                logger_.LogIteration(i, statistics);

                newPopulation.Add(statistics.BestSolution);
                populationHistory_.Add(population_);
                population_ = newPopulation;
            }
            
            return new Result(statistics.BestSolution, statistics.BestScore, evaluator_.Counter);
        }
    }
}