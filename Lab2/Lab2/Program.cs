﻿using System;

namespace Lab2
{
    public static class Program
    {
        private const bool EnhancedRoulette = true;
        private const int InitialPopulation = 1000;
        private const double MutationChance = 0.6;
        private const string Path = @"C:\Users\Raven\Desktop\SI\had12.dat";
        private const int Seconds = 30;
        private static readonly int? seed_ = 350931985;
        private const bool UseRoulette = true;

        public static void Main(string[] args)
        {
            var geneticAlgorithm = new GeneticAlgorithm(EvaluatorLoader.LoadFromFile(Path), new ConsoleLogger(), seed_);
            var result = geneticAlgorithm.Start(Seconds, InitialPopulation, MutationChance, UseRoulette, EnhancedRoulette);
            Console.WriteLine(result);
            Console.ReadLine();
        }
    }
}