﻿using System;
using System.IO;
using System.Linq;

namespace Lab2
{
    public static class EvaluatorLoader
    {
        private const int FlowMatrixStartLine = 2;
        private const int SizeLine = 0;

        public static Evaluator LoadFromFile(string _filePath)
        {
            if (string.IsNullOrEmpty(_filePath))
                throw new ArgumentNullException(nameof(_filePath), "Ścieżka do pliku nie może być pusta!");

            var fileLines = File.ReadAllLines(_filePath);
            var matrixSize = Convert.ToInt32(fileLines[SizeLine].Split(' ').First(_s => _s.Length != 0));

            var flowMatrix = new int[matrixSize, matrixSize];
            var locationMatrix = new int[matrixSize, matrixSize];

            var flowMatrixEndLine = FlowMatrixStartLine + matrixSize - 1;

            for (var i = FlowMatrixStartLine; i <= flowMatrixEndLine; i++)
            {
                var lineValues = fileLines[i].Split(' ').Where(_s => _s.Length != 0).ToList();
                if (lineValues.Count != matrixSize)
                    throw new FileLoadException("Plik jest nieprawidłowy!");
                for (var j = 0; j < matrixSize; j++)
                    flowMatrix[i - FlowMatrixStartLine, j] = Convert.ToInt32(lineValues[j]);
            }

            var locationMatrixStartLine = flowMatrixEndLine + 2;
            var locationMatrixEndLine = locationMatrixStartLine + matrixSize - 1;

            for (var i = locationMatrixStartLine; i <= locationMatrixEndLine; i++)
            {
                var lineValues = fileLines[i].Split(' ').Where(_s => _s.Length != 0).ToList();
                if (lineValues.Count != matrixSize)
                    throw new FileLoadException("Plik jest uszkodzony!");
                for (var j = 0; j < matrixSize; j++)
                    locationMatrix[i - locationMatrixStartLine, j] = Convert.ToInt32(lineValues[j]);
            }

            return new Evaluator(flowMatrix, locationMatrix);
        }
    }
}