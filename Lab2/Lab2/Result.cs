﻿using System.Text;

namespace Lab2
{
    public class Result
    {
        public int EvaluationCounter { get; set; }
        public int Score { get; set; }
        public int[] Solution { get; set; }

        public Result(int[] _solution, int _score, int _evaluationCounter)
        {
            Solution = _solution;
            Score = _score;
            EvaluationCounter = _evaluationCounter;
        }

        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.Append("Solution: ");
            builder.Append(Solution.IntArrayToString());
            builder.Append("\tScore: ");
            builder.Append(Score);
            builder.Append("\tEvaluationCounter: ");
            builder.Append(EvaluationCounter);
            return builder.ToString();
        }
    }
}