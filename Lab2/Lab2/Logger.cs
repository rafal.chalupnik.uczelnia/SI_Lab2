﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Lab2
{
    public interface ILogger
    {
        void Close();
        void LogIteration(int _iteration, Statistics _statistics);
    }

    public class ConsoleLogger : ILogger
    {
        public void Close()
        {
            Console.WriteLine("Log closed!");
        }
        public void LogIteration(int _iteration, Statistics _statistics)
        {
            if (_statistics == null)
                throw new ArgumentNullException(nameof(_statistics), "Statistics cannot be null!");

            Console.WriteLine($"Iteration:\t{_iteration}");
            Console.WriteLine($"Best score:\t{_statistics.BestScore}");
            Console.WriteLine($"Average score:\t{_statistics.AverageScore}");
            Console.WriteLine($"Worse score:\t{_statistics.WorseScore}");
            Console.WriteLine();
        }
    }

    public class FileLogger : ILogger
    {
        private readonly List<string> fileLines_;
        private readonly string filePath_;

        public FileLogger(string _filePath)
        {
            if (string.IsNullOrEmpty(_filePath))
                throw new ArgumentNullException(nameof(_filePath), "FilePath cannot be null or empty!");
            filePath_ = _filePath;
            fileLines_ = new List<string>();
        }

        public void Close()
        {
            File.WriteAllLines(filePath_, fileLines_);
            fileLines_.Clear();
        }
        public void LogIteration(int _iteration, Statistics _statistics)
        {
            fileLines_.Add($"{_iteration},{_statistics.BestScore},{_statistics.AverageScore},{_statistics.WorseScore}");
        }
    }
}