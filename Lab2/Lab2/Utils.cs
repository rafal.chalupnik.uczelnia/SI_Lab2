﻿using System.Linq;
using System.Text;

namespace Lab2
{
    public static class Utils
    {
        public static bool AreIntArraysEqual(int[] _array1, int[] _array2)
        {
            if (_array1 == null && _array2 == null)
                return true;
            if (_array1 == null || _array2 == null)
                return false;
            if (_array1.Length != _array2.Length)
                return false;
            
            return !_array1.Where((_t, _i) => _t != _array2[_i]).Any();
        }
        public static string IntArrayToString(this int[] _array)
        {
            if (_array == null)
                return "null";

            var builder = new StringBuilder();
            foreach (var t in _array)
            {
                builder.Append(t);
                builder.Append(";");
            }
            return builder.ToString();
        }
        public static int[] Swap(int[] _array, int _index1, int _index2)
        {
            var copiedArray = new int[_array.Length];
            for (var i = 0; i < _array.Length; i++)
                copiedArray[i] = _array[i];

            var index1Value = copiedArray[_index1];
            copiedArray[_index1] = copiedArray[_index2];
            copiedArray[_index2] = index1Value;
            return copiedArray;
        }
    }
}