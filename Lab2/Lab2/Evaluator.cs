﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lab2
{
    public class Evaluator
    {
        private readonly List<Tuple<int[], int>> cache_;
        private readonly int[,] flowMatrix_;
        private readonly int[,] locationMatrix_;

        private int CalculateScore(IEnumerable<int> _solution)
        {
            var list = _solution.ToList();
            var cost = 0;

            for (var r = 0; r < Size; r++)
            {
                for (var c = 0; c < Size; c++)
                {
                    cost += flowMatrix_[r, c] * locationMatrix_[list.IndexOf(r), list.IndexOf(c)];
                }
            }

            return cost;
        }

        public Evaluator(int[,] _flowMatrix, int[,] _locationMatrix)
        {
            if (_flowMatrix == null)
                throw new ArgumentNullException(nameof(_flowMatrix), "FlowMatrix cannot be null!");
            if (_locationMatrix == null)
                throw new ArgumentNullException(nameof(_locationMatrix), "LocationMatrix cannot be null!");

            if (_flowMatrix.GetLength(0) != _flowMatrix.GetLength(1))
                throw new FormatException("FlowMatrix must be square!");
            if (_locationMatrix.GetLength(0) != _locationMatrix.GetLength(1))
                throw new FormatException("LocationMatrix must be square!");

            if (_flowMatrix.Length != _locationMatrix.Length)
                throw new FormatException("FlowMatrix and LocationMatrix must have the same sizes!");

            cache_ = new List<Tuple<int[], int>>();
            flowMatrix_ = _flowMatrix;
            locationMatrix_ = _locationMatrix;
            Size = _flowMatrix.GetLength(0);
        }

        public int Counter { get; private set; }
        public int Size { get; }

        public int Evaluate(int[] _solution)
        {
            foreach (var tuple in cache_)
            {
                if (Utils.AreIntArraysEqual(tuple.Item1, _solution))
                    return tuple.Item2;
            }

            var score = CalculateScore(_solution);
            Counter++;
            cache_.Add(Tuple.Create(_solution, score));
            return score;
        }
    }
}