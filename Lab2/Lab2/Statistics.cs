﻿namespace Lab2
{
    public class Statistics
    {
        public int AverageScore { get; }
        public int BestScore { get; }
        public int[] BestSolution { get; }
        public int WorseScore { get; }

        public Statistics(int[] _bestSolution, int _bestScore, int _averageScore, int _worseScore)
        {
            BestSolution = _bestSolution;
            BestScore = _bestScore;
            AverageScore = _averageScore;
            WorseScore = _worseScore;
        }
    }
}